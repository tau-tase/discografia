# Discografia - the Distributed Framework for Service Choreography Execution 

This software project includes contributions from multiple authors:
* Discografia: Petri Kannisto from Tampere University
* The algorithms Smart Routing and Dynamic Pricing: Erdem Gümrükcü and others from RWTH Aachen University
* `wait-for-it` script for Docker containers: Giles Hall

---

This work was supported by the projects
"Distributed Management of Electricity System" (DisMa; Academy of Finland grant no. 322676)
and
"Functional ICT Interoperability in Digital Energy Systems for Green Transition" (from Walter Ahlström Foundation, application 20220033 in program "Tutkijat maailmalle").

---


## Purpose

Discografia is a distributed framework for the execution of service choreographies.
This project shows a limited proof of concept for the idea, contributing to the functional interoperability of distributed, service-based systems.

It is recommended to read this article for more information, including illustrations:

P. Kannisto, E. Gümrükcü, F. Ponci, A. Monti, S. Repo, and D. Hästbacka, "Distributed Service Choreography Framework for Interoperability Among Prosumers and Electric Power System", *IEEE Access*, vol. 11, pp. 137969-137989, 2023. [doi:10.1109/ACCESS.2023.3339766](https://doi.org/10.1109/ACCESS.2023.3339766) (available locally here: [kann23.pdf](kann23.pdf))

Discografia supports the distributed execution with no centralized coordinator or orchestrator.
That is, Discografia does not execute anything but each participant runs it own share of the choreography instead.
For the execution, Discografia provides supportive services, such as choreography distribution, authorization, and monitoring.

The use case is the coordination of the charging of electric vehicles (EV) in urban charging hubs (UCH).
The following actors are present: customer, UCH broker, aggregators, and distribution system operator (DSO).
See the figure below for an illustration in Business Process Model and Notation (BPMN).
This very same process is executed in the demo.

![Process illustrated](uch_distr_actual.svg)

BPMN models are machine-readable and -executable, and therefore UCH broker parses and executes the process model.
To save some time in the implementation, the other actors run the same process in a hard-coded manner, whereas UCH broker has hard-coded only the execution of the individual BPMN elements but not the flow.
In a future implementation, more of BPMN-model-based execution can be included.
BPMN contributes to functional interoperability because the parties of the collaborative process no longer need to communicate the details of the process manually.
The parties can either execute the BPMN model or develop the respective software manually.
Either way, the amount of manual communication is reduced.
BPMN is a platform- and vendor-independent standard.


## How to run

### System requirements

The version used in development is given in parentheses.

* Linux (Ubuntu 20.04 LTS as virtual machine); may work in Windows too
* Docker (20.10.23)
* Docker Compose (2.15.1)
* Docker commands set up to run without `sudo`
    * see https://docs.docker.com/engine/install/linux-postinstall/
* Python (3.11.1)
* Java Development Kit (JDK) (OpenJDK 17.0.5)
* Maven (3.6.3)
* 9 GB of free disk space for Docker images
    * 1 GB for Python
    * 0.5 GB for Java
    * 0.5 GB for HiveMQ server
    * 5.7 GB for Anaconda
    * 1 GB for application builds (estimate)
* _n_ GB of RAM (6 GB)
* _n_ CPU cores (3)


### Commands

In the folder `discografia`, run the following commands.

```
$ ./build.sh
$ docker compose up
```

The build command can take several minutes due to the Anaconda-based Smart Routing algorithm.

Additionally, the subsequent execution took 30-60 seconds in the development environment.
In real life, this process would be fast, but this demo is slow due to all actors running as Docker containers in the same computer and because each container must be started first.

The execution has finished when the customer container has quit.
You can verify quitting by giving the following command and searching for the status of `discografia-customer-1`.
```
$ docker ps -a
```

You can examine the events of the workflow by looking at the log of each actor.
In addition, the container `discografia-framework-monitoring` logs all the messages exchanged between the actors.
You can view these with the command below.
```
$ docker logs discografia-framework-monitoring-1
```


## Development environment

There are likely no strict requirements, but the environment was as described below.

* Ubuntu 20.04 (virtual machine)
* For Python and Docker
    * Visual Studio Code 1.74
* For Java
    * Eclipse 2021-12 (4.22.0)


## Folders and files

To ensure each author gets the credit they deserve, the system comes with three subfolders:
* `discografia`: the choreography framework
* `dynamic-pricing`: algorithm/service for the aggregators to determine to prices of EV charging
* `smart-routing`: algorithm/service for the UCH broker to select the aggregator and charger
* `wait-for-it`: a script to enable Docker images to wait for another service to come online

Please refer to the license information in each folder!

As the build script runs, it copies the folders to the appropriate locations within the folder `discografia`.

Furthermore, the folder `discografia` contains subfolders to separate software platforms for hygienic reasons during development:
* `python-project`: Python files (made with Visual Studio Code)
* `java-workspace`: Java files (made with Eclipse)


## Docker design

The images `customer`, `aggregator`, and `dso` build upon the image `pythonbase`.
This provides some common functionality to all Python clients.

The script `wait-for-it` enables the containers to proceed with starting only after the necessary services are online.
This has been set up as follows:

* `framework-endpoint` and `framework-monitoring` wait for `mqtt-server`
* `customer`, `uch-broker`, `aggregator`, and `dso` wait for `framework-endpoint`


## Python setup for development

You can use a virtual environment to run the Python code locally without building the Docker image.
To create the virtual environment required for development, use the file `requirements.txt`.

On the other hand, to generate the requirements file (in the folder where `.venv` is located):
```
python3 .venv/bin/pip freeze -l > requirements.txt
```

Refer to:
* https://stackoverflow.com/questions/6590688/is-it-bad-to-have-my-virtualenv-directory-inside-my-git-repository
* https://stackoverflow.com/questions/8073097/how-to-freeze-packages-installed-only-in-the-virtual-environment
* https://stackoverflow.com/questions/41427500/creating-a-virtualenv-with-preinstalled-packages-as-in-requirements-txt

