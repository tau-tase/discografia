# README for uchbrokering

## Camunda

Getting started: [https://docs.camunda.org/get-started/java-process-app/project-setup/](https://docs.camunda.org/get-started/java-process-app/project-setup/)

## Maven

In Maven build, better not run the tests because these need network resources to run.

For example, you can build with this command:
```
mvn clean compile assembly:single
```
