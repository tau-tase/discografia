// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;

/**
 * Prepares the application to run the choreography.
 * @author Petri Kannisto
 */
class FrameworkServiceWrapper implements AutoCloseable
{
    private final String m_tokenHeader;
    
    private RestClient m_restClient = null;
    private String m_mqttHost = null;
    
    
    /**
     * Creates an instance.
     * @return Instance.
     * @throws Thrown if an error occurs.
     */
    public static FrameworkServiceWrapper createInstance() throws Exception
    {
        // Authorizing with the choreo framework and discovering the
        // MQTT broker for communication. In "real life", the brokers would 
        // likely be specific to regions or business parties, which means
        // that a scheme as simple as here would be insufficient for 
        // interoperability.
        
        // Try to get the framework host from an environment variable
        String frameworkHost = System.getenv("DEPENDENCY_HOST");
        
        if (frameworkHost == null || frameworkHost == "")
        {
            frameworkHost = "127.0.0.1";
        }
        
        RestClient restClient = null;
        
        try
        {
            // Creating a Rest client
            restClient = RestClient.createInstance(frameworkHost);
        }
        catch (Exception e)
        {
            if (restClient != null)
            { restClient.close(); }
            
            throw new Exception("Failed to create Rest client", e);
        }
        
        // Authorization
        String tokenHeader = restClient.postExpectJson("/authorization", "examplekey-1234").getString("token");
        
        // Retrieve MQTT host
        //String mqttHost = restClient.getExpectJson("/brokers/mqtt", tokenHeader).getString("host");
        
        return new FrameworkServiceWrapper(restClient, tokenHeader);
    }
    
    private FrameworkServiceWrapper(RestClient restClient, String tokenHeader)
    {
        m_restClient = restClient;
        m_tokenHeader = tokenHeader;
    }
    
    /**
     * MQTT host.
     * @return MQTT host.
     * @throws Exception Thrown if the operation fails.
     */
    public String getMqttHost() throws Exception
    {
        if (m_mqttHost == null)
        {
            // Retrieve MQTT host
            m_mqttHost = m_restClient.getExpectJson("/brokers/mqtt", m_tokenHeader).getString("host");
        }
        
        return m_mqttHost;
    }
    
    /**
     * Choreography model.
     * @return Choreography model.
     * @throws Exception Thrown if the operation fails.
     */
    public BpmnModelInstance getChoreoModel() throws Exception
    {
        return (BpmnModelInstance)m_restClient.get(
                "/choreographies/models/uch_brokering", m_tokenHeader,
                stream -> {
                    return Bpmn.readModelFromStream(stream);
                });
    }

    @Override
    public void close() throws Exception
    {
        if (m_restClient != null)
        {
            m_restClient.close();
            m_restClient = null;
        }
    }
}
