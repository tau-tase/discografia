// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

/**
 * Holds charging unit ID information.
 * @author Petri Kannisto
 */
public class ChargingUnitId implements Comparable<ChargingUnitId>
{
   // The interface Comparable enables this to be used as the key of SortedMap
    
    private final String m_fullId;
    private final String m_aggregatorId;
    
    
    /**
     * Creates an instance.
     * @param fullId Full charging unit ID as string.
     * @return Instance.
     */
    public static ChargingUnitId createInstance(String fullId) throws Exception
    {
        // Assuming the format "cu_<aggregator_id>_<other_id_information>"
        
        if (!fullId.startsWith("cu_"))
        {
            throw new Exception("Unexpected start of charging unit ID");
        }
        
        // Parsing
        String[] parts = fullId.split("_");
        
        if (parts.length != 3)
        {
            throw new Exception("Unexpected format of charging unit ID");
        }
        
        return new ChargingUnitId(fullId, parts[1]);
    }
    
    private ChargingUnitId(String fullId, String aggrId)
    {
        // Private ctor to force instantiation with another method
        m_fullId = fullId;
        m_aggregatorId = aggrId;
    }
    
    /**
     * Full ID.
     * @return Full ID.
     */
    public String getFullId()
    {
        return m_fullId;
    }
    
    /**
     * Aggregator ID.
     * @return Aggregator ID.
     */
    public String getAggregatorId()
    {
        return m_aggregatorId;
    }

    @Override
    public int compareTo(ChargingUnitId o)
    {
        // Comparison based on comparing the full ID
        return m_fullId.compareTo(o.m_fullId);
    }
}
