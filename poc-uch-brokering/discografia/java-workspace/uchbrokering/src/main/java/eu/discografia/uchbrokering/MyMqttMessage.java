// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import java.nio.charset.StandardCharsets;

import org.json.JSONObject;

import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5Publish;


/**
 * Represents an MQTT message.
 * @author Petri Kannisto
 */
class MyMqttMessage
{
    private final static String charsetName = StandardCharsets.UTF_8.name();
    
    private final String m_topic;
    private final JSONObject m_payload;
    
    
    /**
     * Creates an instance.
     * @param msg Message.
     * @return Instance.
     */
    public static MyMqttMessage createInstance(Mqtt5Publish msg)
    {
        byte[] payloadBytes = msg.getPayloadAsBytes();
        JSONObject json = new JSONObject(new String(payloadBytes));
        
        return new MyMqttMessage(msg.getTopic().toString(), json);
    }
    
    /**
     * Creates an instance.
     * @param topic Topic name or pattern.
     * @param payload
     * @return Instance.
     */
    public static MyMqttMessage createInstance(String topic, JSONObject payload)
    {
        return new MyMqttMessage(topic, payload);
    }
    
    private MyMqttMessage(String topic, JSONObject payload)
    {
        m_topic = topic;
        m_payload = payload;
    }
    
    /**
     * The full topic name when receiving, or the topic pattern when publishing.
     * @return Topic name or pattern.
     */
    public String getTopic()
    {
        return m_topic;
    }
    
    /**
     * Payload.
     * @return Payload.
     */
    public JSONObject getPayloadAsJson()
    {
        return m_payload;
    }
    
    /**
     * Payload.
     * @return Payload. 
     * @throws Exception Thrown if the operation fails. 
     */
    public byte[] getPayloadAsBytes() throws Exception
    {
        // In toString(), supplying the "indentFactor" parameter to facilitate
        // debugging with the messages
        return m_payload.toString(2).getBytes(charsetName);
    }
}
