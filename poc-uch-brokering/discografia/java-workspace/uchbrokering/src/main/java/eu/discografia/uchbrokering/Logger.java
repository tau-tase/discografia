// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

/**
 * Adds log entries.
 * @author Petri Kannisto
 */
class Logger
{
    /**
     * Constructor.
     */
    public Logger()
    {
        // Empty ctor body
    }
    
    /**
     * Logs a message.
     * @param msg Message.
     */
    public void log(String msg)
    {
        // Just printing to the standard output
        System.out.println("[UCH broker] " + msg);
    }
    
    /**
     * Logs an exception.
     * @param e Exception.
     */
    public void logException(Exception e)
    {
        e.printStackTrace();
    }
}
