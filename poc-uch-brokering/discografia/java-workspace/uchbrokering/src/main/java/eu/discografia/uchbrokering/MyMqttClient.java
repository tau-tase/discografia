// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.hivemq.client.mqtt.MqttGlobalPublishFilter;
import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient;
import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient.Mqtt5Publishes;
import com.hivemq.client.mqtt.mqtt5.Mqtt5Client;
import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5Publish;

/**
 * MQTT client wrapper class. The names starts with "my" not to collide with classes
 * in libraries.
 * The implementation is not completely robust, because if any message other than expect
 * is received when waiting, it will be discarded.
 * @author Petri Kannisto
 */
class MyMqttClient implements AutoCloseable
{
    // For code examples, see https://github.com/hivemq/hivemq-mqtt-client
    
    private final Logger m_logger;
    private Mqtt5BlockingClient m_client = null;
    private Mqtt5Publishes m_publishes = null;
    
    
    /**
     * Creates an instance and open a connection to the server.
     * @param logger Logger.
     * @param mqttHost Host.
     * @return Instance.
     * @throws Exception Thrown if the connection fails.
     */
    public static MyMqttClient createInstanceAndConnect(Logger logger, String mqttHost) throws Exception
    {
        try
        {
            // Using the blocking API
            Mqtt5Client client = Mqtt5Client.builder().serverHost(mqttHost).build();
            Mqtt5BlockingClient blockingClient = client.toBlocking();
            blockingClient.connect();
            Mqtt5Publishes publishes = blockingClient.publishes(MqttGlobalPublishFilter.SUBSCRIBED);
            
            return new MyMqttClient(logger, blockingClient, publishes);
        }
        catch (Exception e)
        {
            throw new Exception("Failed to connect to " + mqttHost + ": " + e.getMessage(), e);
        }
    }
    
    private MyMqttClient(Logger logger, Mqtt5BlockingClient client, Mqtt5Publishes publishes)
    {
        m_logger = logger;
        m_client = client;
        m_publishes = publishes;
    }
    
    /**
     * Publishes a message.
     * @param msg Message.
     * @throws Exception Thrown if the operation fails.
     */
    public void publishMessage(MyMqttMessage msg) throws Exception
    {
        m_client.publishWith().topic(msg.getTopic()).payload(msg.getPayloadAsBytes()).send();
    }
    
    /**
     * Subscribes to given topic filter.
     * @param topic Topic filter.
     * @throws Exception Thrown if the operation fails.
     */
    public void subscribeToTopicFilter(String topic) throws Exception
    {
        m_client.subscribeWith().topicFilter(topic).send();
    }
    
    /**
     * Waits until a message arrives from given topic. The topic must be already subscribed earlier!
     * If no message arrives within until a timeout happens, an exception is thrown.
     * @param topicEnd The expected ending of the topic to receive from.
     * @return The received message.
     * @throws Exception Thrown if the operation fails.
     */
    public MyMqttMessage requireMessage(String topicEnd) throws Exception
    {
        return requireMessage(topicEnd, 0);
    }
    
    /**
     * Waits until a message arrives from given topic. The topic must be already subscribed earlier!
     * If no message arrives within until a timeout happens, an exception is thrown.
     * @param topicSubstr A substring that must appear in the topic name.
     * @param timeOut Timeout in milliseconds.
     * @return The received message.
     * @throws Exception Thrown if the operation fails.
     */
    public MyMqttMessage requireMessage(String topicSubstr, int timeOut) throws Exception
    {
        return waitForMessage(topicSubstr, timeOut, true);
    }
    
    /**
     * Waits until a message arrives from given topic. The topic must be already subscribed earlier!
     * If no message is received, null is returned.
     * @param topicSubstr A substring that must appear in the topic name.
     * @param timeOut Timeout in milliseconds.
     * @return The received message, or null if no message matching the topic has been received.
     * @throws Exception Thrown if the operation fails.
     */
    public MyMqttMessage receiveMessageMaybe(String topicSubstr, int timeOut) throws Exception
    {
        return waitForMessage(topicSubstr, timeOut, false);
    }
    
    private MyMqttMessage waitForMessage(String topicSubstr, int timeOut, boolean require) throws Exception
    {
        // If no timeout provided, 15 seconds is the default
        timeOut = timeOut < 1 ? 15000 : timeOut;
        
        // This will loop forever if any messages are received to any of the subscribed topics!
        // Not the best design really...
        while (true)
        {
            // Waiting at most until the given timeout
            Optional<Mqtt5Publish> maybe = m_publishes.receive(timeOut, TimeUnit.MILLISECONDS);
            
            if (maybe.isEmpty())
            {
                if (require)
                {
                    throw new Exception("Timeout while waiting for message, expected from topic containing '" + topicSubstr + "'");
                }
                
                return null; // Message not required
            }
            
            Mqtt5Publish message = maybe.get();
            MyMqttMessage retval = MyMqttMessage.createInstance(message);
            String actualTopic = retval.getTopic();
            
            // Retry if unexpected topic
            if (!actualTopic.contains(topicSubstr))
            {
                m_logger.log("Received message from unexpected topic: '" + actualTopic + "'");
                continue;
            }
            
            return retval;
        }
    }
    
    @Override
    public void close() throws Exception
    {
        if (m_publishes != null)
        {
            try
            {
                m_publishes.close();
                m_publishes = null;
            }
            catch (Exception e)
            {} // No can do!
        }
        
        if (m_client != null)
        {
            try
            {
                m_client.disconnect();
                m_client = null;
            }
            catch (Exception e)
            {} // No can do!
        }
    }
}
