// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Function;

import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.http.io.support.ClassicRequestBuilder;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * Implements a Rest client.
 * @author Petri Kannisto
 */
class RestClient implements AutoCloseable
{
    private final String authorizationHeaderName = "Authorization";
    
    private final String m_host;
    
    private CloseableHttpClient m_httpClient = null;
    
    
    /**
     * Creates an instance.
     * @param host Rest service host.
     * @return Instance.
     */
    public static RestClient createInstance(String host)
    {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        return new RestClient(host, httpClient);
    }
    
    private RestClient(String host, CloseableHttpClient httpClient)
    {
        m_host = host;
        m_httpClient = httpClient;
    }
    
    /**
     * Performs a POST operation, expecting JSON in the response.
     * No payload will be sent!
     * @param resourcePath The path of the resource.
     * @param authHeader Authorization header.
     * @return Response body.
     * @throws IOException Thrown if the operation fails.
     */
    public JSONObject postExpectJson(String resourcePath, String authHeader) throws IOException
    {
        // Code example: https://hc.apache.org/httpcomponents-client-5.2.x/quickstart.html
        
        ClassicHttpRequest postRequest = ClassicRequestBuilder.
                post(buildRequestUrl(resourcePath)).
                addHeader(authorizationHeaderName, authHeader).
                build();
        
        return (JSONObject)executeRequest(postRequest,
                stream -> {
                    JSONTokener tokener = new JSONTokener(stream);
                    return new JSONObject(tokener);
                });
    }
    
    /**
     * Performs a GET operation, expecting JSON in the response.
     * @param resourcePath The path of the resource.
     * @param authHeader Authorization header.
     * @return Response body.
     */
    public JSONObject getExpectJson(String resourcePath, String authHeader) throws IOException
    {
        return (JSONObject)get(resourcePath, authHeader,
                stream -> {
                    JSONTokener tokener = new JSONTokener(stream);
                    return new JSONObject(tokener);
                });
    }
    
    /**
     * Performs a GET operation with an external handler of input streams.
     * @param resourcePath The path of the resource.
     * @param authHeader Authorization header.
     * @return Return value from the given stream handler.
     */
    public Object get(String resourcePath, String authHeader,
            Function<InputStream, Object> handler) throws IOException
    {
        ClassicHttpRequest getRequest = ClassicRequestBuilder.
                get(buildRequestUrl(resourcePath)).
                addHeader(authorizationHeaderName, authHeader).
                build();
        
        return executeRequest(getRequest, handler);
    }

    @Override
    public void close() throws Exception
    {
        if (m_httpClient != null)
        {
            m_httpClient.close();
            m_httpClient = null;
        }
    }
    
    private Object executeRequest(ClassicHttpRequest request,
            Function<InputStream, Object> handler) throws IOException
    {
        return m_httpClient.execute(request, response ->
        {
            if (response.getCode() != 200)
            {
                throw new IOException("Unexpected status code " + response.getCode());
            }
            
            try (InputStream inputStream = response.getEntity().getContent())
            {
                return handler.apply(inputStream);
            }
        });
    }
    
    private String buildRequestUrl(String path)
    {
        return "http://" + m_host + path;
    }
}
