// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;


/**
 * This implements the UCH brokering service for the choreography execution demo.
 */
public class App 
{
    public static void main(String[] args)
    {
        Logger logger = new Logger();
        logger.log("Hello from UCH brokering service!");
        
        FrameworkServiceWrapper framework = null;
        
        try
        {
            framework = FrameworkServiceWrapper.createInstance();
            WorkflowExecutor executor = WorkflowExecutor.createInstance(logger, framework);
            executor.execute();
        }
        catch (Exception e)
        {
            logger.logException(e);
        }
        
        try {
            if (framework != null)
            { framework.close(); }
        }
        catch (Exception e) {}
    }
}
