// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import java.time.Instant;
import java.util.Set;
import java.util.SortedMap;

import org.json.JSONObject;


/**
 * Represents a request for smart routing.
 * @author Petri Kannisto
 */
public class SmartRoutingRequest
{
    private JSONObject m_json = new JSONObject();
    
    
    /**
     * Creates an instance.
     * @param custReq Request from the customer.
     * @param offers Offers from aggregators.
     * @return Instance.
     */
    public static SmartRoutingRequest createInstance(JSONObject custReq, SortedMap<ChargingUnitId, JSONObject> offers)
            throws Exception
    {
        SmartRoutingRequest instance = new SmartRoutingRequest();
        Set<ChargingUnitId> chargerIds = offers.keySet();
        long arrivalTimeUnix = instance.isoTimeToUnix(custReq.getString("time_arrival"));
        long departureTimeUnix = instance.isoTimeToUnix(custReq.getString("time_departure"));
        
        // Optimization resolution, 300 s -> 5 minutes
        instance.m_json.put("opt_step", 300);
        
        // Energy capacity of battery (kWs/kilowatt seconds)
        instance.m_json.put("ecap", custReq.getDouble("battery_capacity_kwh") * 3600);
        
        // V2G allowance discharge (kWs/kilowatt seconds)
        instance.m_json.put("v2gall", custReq.getDouble("v2g_allowance_kwh") * 3600);
        
        // Target final soc (0<inisoc<1)
        instance.m_json.put("tarsoc", custReq.getDouble("soc_target"));
        
        // Arrival state-of-charge for each aggregator
        // Just blindly assigning the arrival SOC rather than looking what it is for each charging unit
        instance.assignDoubleForAggregators("arrsoc", chargerIds, custReq.getDouble("soc_arrival"));
        
        // Start and end time for optimization
        instance.m_json.put("opt_horizon_start", arrivalTimeUnix);
        instance.m_json.put("opt_horizon_end", departureTimeUnix);
        
        // Arrival and departure time; simply using the same time for all aggregators.
        // Must use long instead of double because double would result in a JSON value ending
        // with the exponent "E9" (e.g., 1.6784658E9), which does not work in the service being invoked.
        instance.assignLongForAggregators("arrtime", chargerIds, arrivalTimeUnix);
        instance.assignLongForAggregators("deptime", chargerIds, departureTimeUnix);
        
        instance.assignOfferSpecificData(offers);
        
        return instance;
    }
    
    private long isoTimeToUnix(String input)
    {
        return Instant.parse(input).getEpochSecond();
    }
    
    private void assignDoubleForAggregators(String field, Set<ChargingUnitId> chargingUnitIds, double value)
    {
        // Creating a child JSON object instead of using a map to ensure that the
        // aggregator IDs remain in the original order
        JSONObject child = new JSONObject();
        
        for (ChargingUnitId aggr : chargingUnitIds)
        {
            child.put(aggr.getAggregatorId(), value);
        }
        
        m_json.put(field, child);
    }
    
    private void assignLongForAggregators(String field, Set<ChargingUnitId> chargingUnitIds, long value)
    {
        // Creating a child JSON object instead of using a map to ensure that the
        // aggregator IDs remain in the original order
        JSONObject child = new JSONObject();
        
        for (ChargingUnitId aggr : chargingUnitIds)
        {
            child.put(aggr.getAggregatorId(), value);
        }
        
        m_json.put(field, child);
    }
    
    private void assignOfferSpecificData(SortedMap<ChargingUnitId, JSONObject> offers)
    {
        // Nominal charging  and discharging power
        m_json.put("p_ch", pickFieldForAllAggregators("p_ch_max", offers));
        m_json.put("p_ds", pickFieldForAllAggregators("p_ds_max", offers));
        
        // Candidate_chargers
        m_json.put("candidate_chargers", pickFieldForAllAggregators("charger_id", offers));
        
        // Prices: grid to vehicle and vehicle to grid
        m_json.put("dps_g2v", pickFieldForAllAggregators("dps_g2v", offers));
        m_json.put("dps_v2g", pickFieldForAllAggregators("dps_v2g", offers));
    }
    
    private JSONObject pickFieldForAllAggregators(String field, SortedMap<ChargingUnitId, JSONObject> offers)
    {
        JSONObject retval = new JSONObject();
        
        for (ChargingUnitId key : offers.keySet())
        {
            JSONObject off = offers.get(key);
            retval.put(key.getAggregatorId(), off.get(field));
        }
        
        return retval;
    }
    
    private SmartRoutingRequest()
    {
        // Empty ctor body
    }
    
    /**
     * Request as JSON.
     * @return Request as JSON.
     */
    public JSONObject getJson()
    {
        return m_json;
    }
}
