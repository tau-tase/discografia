// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import java.util.Collection;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.FlowNode;
import org.camunda.bpm.model.bpmn.instance.SequenceFlow;
import org.camunda.bpm.model.bpmn.instance.StartEvent;

/**
 * Executes the BPMN workflow of one participant.
 * @author Petri Kannisto
 */
class WorkflowExecutor
{
    // High-level functionality:
    // 1) Setup: authorization, get MQTT broker, get choreography specification
    // 2) Wait for a request to come
    // 3) Run the workflow
    // 4) Exit
    //
    // In real life, this service would run continuously, serving requests.
    
    /* Help regarding Camunda:
     * https://docs.camunda.org/get-started/java-process-app/project-setup/
     * https://docs.camunda.org/manual/latest/user-guide/model-api/bpmn-model-api/read-a-model/
     */
    
    private final Logger m_logger;
    private final FrameworkServiceWrapper m_framework;
    
    
    /**
     * Creates an instance.
     * @param logger Logger.
     * @param framework Framework services.
     * @return Instance.
     */
    public static WorkflowExecutor createInstance(Logger logger, FrameworkServiceWrapper framework)
    {
        return new WorkflowExecutor(logger, framework);
    }
    
    private WorkflowExecutor(Logger logger, FrameworkServiceWrapper framework)
    {
        m_logger = logger;
        m_framework = framework;
    }
    
    /**
     * Executes the workflow.
     * @throws Exception Thrown if the workflow fails.
     */
    public void execute() throws Exception
    {
        try (MyMqttClient mqttClient = MyMqttClient.createInstanceAndConnect(
                m_logger, m_framework.getMqttHost()))
        {
            WorkflowNodeExecutor nodeExecutor = WorkflowNodeExecutor.createInstance(
                    m_logger, mqttClient);
            
            // Getting choreography specification from the choreo framework.
            // In "real life", the spec would likely be cached and re-used.
            BpmnModelInstance choreoModel = m_framework.getChoreoModel();
            log("Choreo model retrieved and parsed.");
            
            // Starting with the start event
            StartEvent uchBrokerStart = (StartEvent)choreoModel.getModelElementById(
                    WorkflowNodeExecutor.nodeId_eventStart);
            executeSteps(nodeExecutor, uchBrokerStart);
        }
    }
    
    private void executeSteps(WorkflowNodeExecutor engine, StartEvent uchBrokerStart) throws Exception
    {
        FlowNode currentNode = uchBrokerStart;
        
        log("Executing workflow of UCH broker.");
        
        // Execute nodes until none left
        while (true)
        {
            log(String.format("--- Now executing '%s' (%s)", currentNode.getName(), currentNode.getId()));
            engine.executeNode(currentNode);
            
            // What comes next?
            Collection<SequenceFlow> outgoingFlows = currentNode.getOutgoing();
            
            if (outgoingFlows.isEmpty())
            {
                log("No further nodes left to execute!");
                break;
            }
            
            // Assuming there is only one flow from this node!
            // This wouldn't work with gateways.
            SequenceFlow firstFlow = outgoingFlows.iterator().next();
            currentNode = firstFlow.getTarget();
        }
        
        log("Workflow of UCH broker finished.");
    }
    
    private void log(String msg)
    {
        logStatic(m_logger, msg);
    }
    
    private static void logStatic(Logger logger, String msg)
    {
        logger.log(msg);
    }
}
