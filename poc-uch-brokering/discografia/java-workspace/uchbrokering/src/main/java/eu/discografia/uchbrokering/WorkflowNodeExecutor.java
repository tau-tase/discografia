// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import java.util.SortedMap;
import java.util.TreeMap;

import org.camunda.bpm.model.bpmn.instance.FlowNode;
import org.json.JSONObject;

/**
 * This class implements the custom-made BPMN engine logic.
 * In a real, large-scale application, this would be better
 * made with an actual BPMN engine, such as Camunda (Java)
 * or SpiffWorkflow (Python).
 * @author Petri Kannisto
 */
class WorkflowNodeExecutor
{
    /**
     * The ID of the start event for the UCH broker service.
     */
    public static final String nodeId_eventStart = "Event_uch_broker_start";
    
    private static final String nodeId_eventCatch_offerResponse = "Event_uch_broker_catch_0hqbndy";
    private static final String nodeId_eventCatch_reservationResponse = "Event_uch_broker_catch_19amavl";
    private static final String nodeId_eventThrow_offerRequest = "Event_uch_broker_throw_1rd4u2l";
    private static final String nodeId_eventThrow_offerAccept = "Event_uch_broker_throw_0dociz7";
    private static final String nodeId_eventThrow_offerReject = "Event_uch_broker_throw_007ujep";
    private static final String nodeId_eventEnd = "Event_uch_broker_end_1m7hlvs";
    private static final String nodeId_taskChooseUch = "Activity_06kolqs";
    
    // Prefix of all topic names
    private static final String topicNameStart = "discografia";
    
    // Party roles that appear in topics
    private static final String topicRole_aggregator = "Aggregator";
    private static final String topicRole_uchBroker = "UchBroker";
    
    // The message type names that appear in topics
    private static final String topicMsg_uchBroker_reservationRequest = "ReservationRequest";
    private static final String topicMsg_aggregator_offerRequest = "OfferRequest";
    private static final String topicMsg_aggregator_offerResponse = "OfferResponse";
    private static final String topicMsg_aggregator_offerAcceptRequest = "ReservationAcceptRequest";
    private static final String topicMsg_aggregator_offerAcceptResponse = "ReservationAcceptResponse";
    private static final String topicMsg_aggregator_offerRejectRequest = "ReservationRejectRequest";
    private static final String topicMsg_uchBroker_reservationResponse = "ReservationResponse";
    
    private static final String topicFull_smartRoutingRequest = "routing/request/emo";
    private static final String topicFull_smartRoutingResponse = "routing/response/emo";
    
    // The party ID of the UCH broker; hard-coded!
    private static final String topicMyPartyId = "uchbroker1";
    
    private final Logger m_logger;
    private final MyMqttClient m_mqttClient;
    
    // This identifies the choreography instance in case there
    // are multiple choreographies executed in parallel.
    // However, this class does not currently support parallel instances!
    // The correlation ID appears in topic names though.
    private String m_correlationId = null;
    
    // Use this to get messages for any correlation ID.
    // This can only be used in subscription, not reception, because the custom client
    // wants an exact match to a part of the topic name when receiving.
    private static final String topicWildcardOneLevel = "+";
    
    // Store here the request received from customer
    private JSONObject m_receivedRequestFromCustomer = null;
    
    // Store here the raw offers received from the aggregators.
    // Not sure if the order of aggregator names matters, but anyway using SortedMap.
    private SortedMap<ChargingUnitId, JSONObject> m_aggregatorOffers = new TreeMap<>();
    
    // Store here the ID of the selected charger
    private ChargingUnitId m_selectedChargerId = null;
    
    
    /**
     * Creates an instance.
     * @param Logger Logger.
     * @param mqttClient Client object.
     * @return Instance.
     * @throws Thrown if the operation fails.
     */
    public static WorkflowNodeExecutor createInstance(Logger logger, MyMqttClient mqttClient)
            throws Exception
    {
        // Subscribing to the topic pattern of start events
        String topic = buildTopicNameOrPartOfIt(new String[]
                {topicWildcardOneLevel, topicRole_uchBroker, topicMsg_uchBroker_reservationRequest, topicMyPartyId}, true);
        mqttClient.subscribeToTopicFilter(topic);
        
        return new WorkflowNodeExecutor(logger, mqttClient);
    }
    
    private WorkflowNodeExecutor(Logger logger, MyMqttClient mqttClient)
    {
        m_logger = logger;
        m_mqttClient = mqttClient;
    }
    
    /**
     * Executes the given node.
     * @param node Node.
     * @throws Exception Thrown if the execution fails.
     */
    public void executeNode(FlowNode node) throws Exception
    {
        /* The class is based on the idea that all BPMN elements are
         * known beforehand by their type and ID, and the functionality
         * is otherwise hard-coded. This wouldn't be the case if 
         * the logic were executed with a real BPMN engine. */
        
        String elementType = node.getElementType().getTypeName();
        String nodeId = node.getId();
        
        switch (elementType)
        {
            case "startEvent":
                executeStartEvent(nodeId);
                break;
                
            case "intermediateCatchEvent":
                executeIntermediateCatchEvent(nodeId);
                break;
                
            case "intermediateThrowEvent":
                executeIntermediateThrowEvent(nodeId);
                break;
                
            case "endEvent":
                executeEndEvent(nodeId);
                break;
                
            case "task":
            case "scriptTask":
                executeTask(nodeId);
                break;
                
            default:
                throw new Exception("Unexpected element type '" + elementType + "'");
        }
    }
    
    
    // ### Execution of nodes ###
    
    private void executeStartEvent(String nodeId) throws Exception
    {
        if (!nodeId.equals(nodeId_eventStart))
        {
            throw new Exception("Unexpected start event in model: " + nodeId);
        }
        
        m_logger.log("Waiting for a request to start...");
        
        // Just wait for a message. Once received, do nothing with the payload.
        
        // The correlation ID is still unknown, therefore only looking at a
        // part of the topic name.
        String startTopic = buildTopicNameOrPartOfIt(new String[]
                {topicRole_uchBroker, topicMsg_uchBroker_reservationRequest}, false);
        MyMqttMessage message = m_mqttClient.requireMessage(startTopic);
        m_logger.log("Start event received.");
        
        // Getting the correlation ID from the topic pattern
        m_correlationId = getCorrelationIdFromTopic(message.getTopic());
        m_logger.log("Correlation ID is " + m_correlationId);
        
        // Storing the request for later use
        m_receivedRequestFromCustomer = message.getPayloadAsJson();
        
        // Subscribing to all topics of interest.
        // Using a wildcard for aggregator responses.
        String[] recvTopics = {
                startTopic,
                buildTopicNameOrPartOfIt(new String[]
                        {m_correlationId, topicRole_aggregator, topicMsg_aggregator_offerResponse, topicWildcardOneLevel}, true),
                buildTopicNameOrPartOfIt(new String[]
                        {m_correlationId, topicRole_aggregator, topicMsg_aggregator_offerAcceptResponse, topicWildcardOneLevel}, true),
                topicFull_smartRoutingResponse
        };
        
        for (String t : recvTopics)
        {
            m_mqttClient.subscribeToTopicFilter(t);
        }
    }
    
    private void executeIntermediateCatchEvent(String nodeId) throws Exception
    {
        String topicSubstr = null;
        
        switch (nodeId)
        {
            case nodeId_eventCatch_offerResponse:
                
                receiveOffersFromAggregators();
                break;
                
            case nodeId_eventCatch_reservationResponse:
                
                // Wait for the message but do nothing with it here
                topicSubstr = buildTopicNameOrPartOfIt(new String[]
                        {m_correlationId, topicRole_aggregator, topicMsg_aggregator_offerAcceptResponse}, true);
                m_mqttClient.requireMessage(topicSubstr);
                break;
                
            default:
                throw new Exception("Unexpected intermediate catch event in model: " + nodeId);
        }
    }
    
    private void receiveOffersFromAggregators() throws Exception
    {
        // Giving the aggregators time to reply, storing each offer.
        // Waiting for at most 2 seconds in total.
        // The reliable resolution of this function is some 15 ms!
        // (Refer to https://stackoverflow.com/questions/180158/how-do-i-time-a-methods-execution-in-java)
        long startTime = System.currentTimeMillis();
        
        while (System.currentTimeMillis() - startTime < 2000)
        {
            // Using a short timeout value to enable as many messages as possible to arrive
            // before the given maximal total time.
            String topicSubstr = buildTopicNameOrPartOfIt(new String[]
                    {m_correlationId, topicRole_aggregator, topicMsg_aggregator_offerResponse}, true);
            MyMqttMessage message = m_mqttClient.receiveMessageMaybe(topicSubstr, 100);
            
            if (message == null)
            {
                continue;
            }
            
            // The aggregator ID is in the received message.
            // In real applications, the aggregator ID would likely appear in the topic name too
            // to enable more selective reception of offers.
            
            JSONObject offerJson = message.getPayloadAsJson();
            String chargerId = offerJson.getString("charger_id");
            m_logger.log("Received offer for charging unit " + chargerId);
            ChargingUnitId chargerIdParsed = ChargingUnitId.createInstance(chargerId);
            m_aggregatorOffers.put(chargerIdParsed, offerJson); 
        }
        
        m_logger.log("Offers received for chargers total: " + m_aggregatorOffers.size());
        
        if (m_aggregatorOffers.size() < 1)
        {
            throw new Exception("No offers received from aggregators!");
        }
    }
    
    private void executeIntermediateThrowEvent(String nodeId) throws Exception
    {
        String topic = null;
        JSONObject message = null;
        
        switch (nodeId)
        {
            case nodeId_eventThrow_offerRequest:
                
                // Targeted to all aggregators
                topic = buildTopicNameOrPartOfIt(new String[]
                        {m_correlationId, topicRole_aggregator, topicMsg_aggregator_offerRequest}, true);
                
                message = m_receivedRequestFromCustomer;
                break;
                
            case nodeId_eventThrow_offerAccept:
                
                topic = buildTopicNameOrPartOfIt(new String[]
                        {m_correlationId, topicRole_aggregator, topicMsg_aggregator_offerAcceptRequest,
                                m_selectedChargerId.getAggregatorId()}, true);
                message = new JSONObject();
                message.put("response", "accept");
                message.put("charging_unit_id", m_selectedChargerId.getFullId());
                break;
                
            case nodeId_eventThrow_offerReject:
                
                rejectAggregatorsNotAccepted();
                return;
                
            default:
                throw new Exception("Unexpected intermediate event in model: " + nodeId);
        }
        
        m_mqttClient.publishMessage(MyMqttMessage.createInstance(topic, message));
    }
    
    private void rejectAggregatorsNotAccepted() throws Exception
    {
        for (ChargingUnitId chargerId : m_aggregatorOffers.keySet())
        {
            if (chargerId.getFullId().equals(m_selectedChargerId.getFullId()))
            {
                continue; // This aggregator was accepted, skip
            }
            
            // Publish reject message to this aggregator
            String topic = buildTopicNameOrPartOfIt(new String[]
                    {m_correlationId, topicRole_aggregator, topicMsg_aggregator_offerRejectRequest, chargerId.getAggregatorId()}, true);
            JSONObject message = new JSONObject();
            message.put("response", "reject");
            m_mqttClient.publishMessage(MyMqttMessage.createInstance(topic, message));
        }
    }
    
    private void executeEndEvent(String nodeId) throws Exception
    {
        if (!nodeId.equals(nodeId_eventEnd))
        {
            throw new Exception("Unexpected end event in model: " + nodeId);
        }
        
        String topic = buildTopicNameOrPartOfIt(new String[]
                {m_correlationId, topicRole_uchBroker, topicMsg_uchBroker_reservationResponse, topicMyPartyId}, true);
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("charging_unit_id", m_selectedChargerId.getFullId());
        m_mqttClient.publishMessage(MyMqttMessage.createInstance(topic, jsonObj));
    }
    
    private void executeTask(String nodeId) throws Exception
    {
        if (!nodeId.equals(nodeId_taskChooseUch))
        {
            throw new Exception("Unexpected task in model: " + nodeId);
        }
        
        // Invoking the "smart routing" algorithm to select the best offer
        SmartRoutingRequest request = SmartRoutingRequest.createInstance(m_receivedRequestFromCustomer, m_aggregatorOffers);
        m_mqttClient.publishMessage(
                MyMqttMessage.createInstance(topicFull_smartRoutingRequest, request.getJson())
                );
        MyMqttMessage smartRoutingResponse = m_mqttClient.requireMessage(topicFull_smartRoutingResponse);
        
        // Storing the selected charger ID
        String chargerIdRaw = smartRoutingResponse.getPayloadAsJson().getString("Charger");
        m_selectedChargerId = ChargingUnitId.createInstance(chargerIdRaw);
        
        String logMsg = String.format("Selected charger '%s' from aggregator '%s'",
                m_selectedChargerId.getFullId(), m_selectedChargerId.getAggregatorId());
        m_logger.log(logMsg);
    }
    
    
    // ### Processing of topic names ###
    
    private static String buildTopicNameOrPartOfIt(String[] parts, boolean addPrefix)
    {
        // Join the parts, separating by "/"
        
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        
        if (addPrefix)
        {
            sb.append(topicNameStart); // Add the demo-specific prefix
            first = false;
        }
        
        for (String p : parts)
        {
            if (!first)
            {
                sb.append("/");
            }
            
            first = false;
            sb.append(p);
        }
        
        return sb.toString();
    }
    
    private String getCorrelationIdFromTopic(String topic)
    {
        // Remove the common start of all topic names
        String remainder = topic.substring((topicNameStart + "/").length());
        
        // From the remainder, remove anything after the first "/"
        return remainder.substring(0, remainder.indexOf("/"));
    }
}
