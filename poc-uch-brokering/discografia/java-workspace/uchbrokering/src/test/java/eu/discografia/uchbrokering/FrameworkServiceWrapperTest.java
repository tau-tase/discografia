// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import static org.junit.Assert.*;

import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.junit.Test;

public class FrameworkServiceWrapperTest 
{
    // Before running the test, make sure the framework is running
    // at 127.0.0.1:80!
    // E.g.:
    // 1) Navigate to image folder
    // 2) docker build -t choreo-fw-alone .
    // 3) docker run -p 80:80 choreo-fw-alone
    
    @Test
    public void testCreateInstance()
    {
        // Testing that object creation won't fail
        FrameworkServiceWrapper testObj = getTestObject();
        
        try { testObj.close(); }
        catch (Exception e) {}
    }
    
    @Test
    public void testGetMqttHost()
    {
        FrameworkServiceWrapper testObj = getTestObject();
        
        try
        {
            assertEquals("127.0.0.1", testObj.getMqttHost());
        }
        catch (Exception e)
        {
            fail(e.getMessage());
        }
        
        try { testObj.close(); }
        catch (Exception e) {}
    }

    @Test
    public void testGetChoreoModel()
    {
        FrameworkServiceWrapper testObj = getTestObject();
        
        try
        {
            BpmnModelInstance model = testObj.getChoreoModel();
            
            // Asserting that a specific start event is found
            assertNotNull(model.getModelElementById("Event_uch_broker_start"));
        }
        catch (Exception e) // Stream close may cause an exception
        {
            fail("Unexpected exception: " + e.getMessage());
        }
        
        try { testObj.close(); }
        catch (Exception e) {}
    }
    
    private FrameworkServiceWrapper getTestObject()
    {
        try
        {
            return FrameworkServiceWrapper.createInstance();
        }
        catch (Exception e)
        {
            fail(e.getMessage());
            
            // Not expecting this to be reached due to fail()
            // (but the compiler requires this!)
            return null;
        }
    }
}
