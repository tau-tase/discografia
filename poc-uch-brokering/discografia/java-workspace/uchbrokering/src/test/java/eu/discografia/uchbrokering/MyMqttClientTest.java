// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import static org.junit.Assert.*;

import org.json.JSONObject;
import org.junit.Test;

/**
 * Testing that the MQTT client can connect to a server and publish and receive messages.
 * That is, this is not actual unit test due to a requirement of a running MQTT server.
 * @author Petri Kannisto
 */
public class MyMqttClientTest
{
    // Before running the test, make sure there is an MQTT host
    // at 127.0.0.1:1883!
    // You can use this Docker command:
    // docker run -p 1883:1883 hivemq/hivemq4
    
    @Test
    public void testBasic()
    {
        // Testing the basics of communication
        
        Logger logger = new Logger();
        
        try
        {
            try (MyMqttClient client = MyMqttClient.createInstanceAndConnect(logger, "localhost"))
            {
                String topic = "my-topic";
                
                // Subscribing to a topic, then publishing a message and expecting to receive it
                
                client.subscribeToTopicFilter(topic);
                
                JSONObject jsonObjectIn = new JSONObject();
                jsonObjectIn.put("my-key", "my-value");
                client.publishMessage(MyMqttMessage.createInstance(topic, jsonObjectIn));
                
                JSONObject jsonObjectOut = client.requireMessage(topic).getPayloadAsJson();
                
                // Checking that the expected key exists in the JSON object
                assertTrue(jsonObjectOut.toString().contains("my-key"));
            }
        }
        catch (Exception e)
        {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void testReceiveMessageMaybe_nothingReceived()
    {
        // Testing whether a message is required or not
        
        Logger logger = new Logger();
        MyMqttClient client = null;
        String topic = "my-topic";
        
        try
        {
            client = MyMqttClient.createInstanceAndConnect(logger, "localhost");
            client.subscribeToTopicFilter(topic);
        }
        catch (Exception e)
        {
            fail(e.getMessage());
        }
        
        // 1) Not getting a message, but message not required
        MyMqttMessage message1 = null;
        
        try
        {
            message1 = client.receiveMessageMaybe(topic, 1000);
        }
        catch (Exception e)
        {
            fail(e.getMessage()); // Unexpected exception
        }
        
        assertNull(message1);
        
        // The client is not closed properly is all cases, but this would be difficult
        // due to multiple assertions
        try
        {
            client.close();
        }
        catch (Exception e)
        {}
    }
    
    @Test
    public void testRequireMessage_nothingReceived()
    {
        Logger logger = new Logger();
        MyMqttClient client = null;
        String topic = "my-topic";
        
        try
        {
            client = MyMqttClient.createInstanceAndConnect(logger, "localhost");
            client.subscribeToTopicFilter(topic);
        }
        catch (Exception e)
        {
            fail(e.getMessage());
        }
        
        Exception timeoutException = null;
        
        try
        {
            client.requireMessage(topic, 1000);
        }
        catch (Exception e)
        {
            timeoutException = e;
        }
        
        assertNotNull(timeoutException);
        assertTrue(timeoutException.getMessage().startsWith("Timeout while waiting for message, expected from topic containing"));
        
        // The client is not closed properly is all cases, but this would be difficult
        // due to multiple assertions
        try
        {
            client.close();
        }
        catch (Exception e)
        {}
    }
}
