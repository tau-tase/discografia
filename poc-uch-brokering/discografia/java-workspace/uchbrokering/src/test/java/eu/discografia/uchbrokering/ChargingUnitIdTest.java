// SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
//
// SPDX-License-Identifier: MIT

package eu.discografia.uchbrokering;

import static org.junit.Assert.*;

import org.junit.Test;

public class ChargingUnitIdTest
{
    @Test
    public void testUnexpectedStart() 
    {
        expectExpection("Unexpected start of charging unit ID", "xx_aggr1_01");
    }
    
    @Test
    public void testUnexpectedCountOfParts() 
    {
        expectExpection("Unexpected format of charging unit ID", "cu_f");
        expectExpection("Unexpected format of charging unit ID", "cu_aggr1_01_01");
    }
    
    @Test
    public void testTypical() throws Exception 
    {
        ChargingUnitId testObj = ChargingUnitId.createInstance("cu_aggrX_01");
        
        assertEquals("cu_aggrX_01", testObj.getFullId());
        assertEquals("aggrX", testObj.getAggregatorId());
    }
    
    @Test
    public void testCompareTo() throws Exception 
    {
        ChargingUnitId idA1 = ChargingUnitId.createInstance("cu_aggr1_A");
        ChargingUnitId idA2 = ChargingUnitId.createInstance("cu_aggr1_A");
        ChargingUnitId idB = ChargingUnitId.createInstance("cu_aggr2_X");
        
        assertEquals(0, idA1.compareTo(idA2));
        assertTrue(idA1.compareTo(idB) < 0);
        assertTrue(idB.compareTo(idA1) > 0);
    }
    
    private void expectExpection(String expectedMessage, String input)
    {
        String actualMessage = "";
        
        try
        {
            ChargingUnitId.createInstance(input);
        }
        catch (Exception e)
        {
            actualMessage = e.getMessage();
        }
        
        assertEquals(expectedMessage, actualMessage);
    }
}
