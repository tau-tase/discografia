#!/bin/bash

# SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
#
# SPDX-License-Identifier: MIT


# This script executes the preparatory steps required before an execution by Docker Compose.

# Copy the dependencies required for build
cp -r ../smart-routing ./python-project/
cp -r ../dynamic-pricing ./python-project/
cp -r ../wait-for-it ./python-project/pythonbase/
cp -r ../wait-for-it ./python-project/smart-routing/
cp -r ../wait-for-it ./java-workspace/uchbrokering/

# Build the common base image for Python images
docker build -t discografia-pythonbase ./python-project/pythonbase

# For UCH brokering, build the required Java package with Maven
(cd ./java-workspace/uchbrokering;mvn clean compile assembly:single)

# Build with Docker Compose
docker compose build

