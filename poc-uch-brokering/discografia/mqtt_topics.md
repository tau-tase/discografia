
# Topic names

## Structure

The structure of topic names is, each level separated with "/":
1. "discografia"
2. correlation ID
3. party role (of the one providing the service being invoked)
4. message type
5. party ID (of the one providing the service being invoked)

The party ID is not used when sending to all parties of the role!


## Role names

* Aggregator
* UchBroker
* Dso

The customer provides no services and therefore has no role name for the topics.


## Message type names

* ReservationRequest (UCH broker)
* OfferRequest (Aggregator)
* OfferResponse (Aggregator)
* ReservationAcceptRequest (Aggregator)
* ReservationAcceptResponse (Aggregator)
* ReservationRejectRequest (Aggregator)
* ReservationResponse (UCH broker)
* NetBehavior (DSO)


## Examples

To request for offers from all aggregators (the correlation ID being "corr1234"):
```
discografia/corr1234/Aggregator/OfferRequest
```

Respectively, the response from aggregator "AggregatorA" is published at:
```
discografia/corr1234/Aggregator/OfferResponse/AggregatorA
```

