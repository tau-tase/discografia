# SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
#
# SPDX-License-Identifier: MIT

import os
import time
import uuid
from fastapi import FastAPI, Header, HTTPException, status
from fastapi.responses import FileResponse
from pydantic import BaseModel

# FastAPI help:
# - headers: https://fastapi.tiangolo.com/tutorial/header-params
# - requests: https://fastapi.tiangolo.com/tutorial/body/

# In the choreography demo, this message tells the customer
# what it needs to request for UCH charging.
class ChoreographyInstance(BaseModel):
    correlation_id: str
    message_broker_host: str


def check_authorization_token_header(authorization_header: str):
    # In an actual implementation, this method would authorize the 
    # client based on the token. Here, accepting any authorization header
    # starting with "exampletoken".
    if authorization_header is None or not authorization_header.startswith("exampletoken"):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)


def get_mqtt_host():

    # Get MQTT host from environment variable (assumed in Docker), 
    # or use localhost if not set (assumed when developing outside of Docker)
    host_from_env = os.environ.get("DEPENDENCY_HOST")

    if (host_from_env is None):
        return "127.0.0.1"
        
    return host_from_env


app = FastAPI()


# This method authorizes a client, providing a token.
# Although there is no payload, using post instead of get as this
# is considered to start a new "session" that would expire after 
# a certain time period.
@app.post("/authorization")
def authorize(
    # FastAPI maps the "Authorization" header to "authorization"
    authorization: str | None = Header(default=None)
): 
    # In an actual implementation, this method would authorize the 
    # client based on its public key. Here, accepting any 
    # authorization header starting with "examplekey".
    if authorization is None or not authorization.startswith("examplekey"):
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

    # Generating a random token. In real life, this would
    # be some cryptographically strong character sequence rather
    # than a UUID.
    token = "exampletoken-" + str(uuid.uuid4())
    return {"token": token} 


# This method provides the current MQTT broker being used.
# This is mainly meant for services because the customers get this
# information as they request to start a new choreography instance.
@app.get("/brokers/mqtt")
def get_mqtt_broker_method(
    # FastAPI maps the "Authorization" header to "authorization"
    authorization: str | None = Header(default=None)
):
    check_authorization_token_header(authorization)
    
    return {"host": get_mqtt_host()}


# This method starts a new choreography instance for UCH brokering.
# Functionality:
# - authorization of the client
# - discovery (of the MQTT broker)
# - initialization of choreography instance by assigning
#   a correlation ID
@app.post("/choreographies/instances/uch_brokering")
def start_instance(
    # FastAPI maps the "Authorization" header to "authorization"
    authorization: str | None = Header(default=None)
):
    check_authorization_token_header(authorization)

    # Generating a correlation ID. In an actual system, it should be made
    # sure it is random and won't collide with other correlation IDs.
    correlation_id = "corr-" + str(time.time())

    retval = ChoreographyInstance(
        correlation_id = correlation_id,
        message_broker_host = get_mqtt_host()
        )

    return retval


# This method provides the most recent choreography model.
@app.get("/choreographies/models/uch_brokering")
def get_model(
    # FastAPI maps the "Authorization" header to "authorization"
    authorization: str | None = Header(default=None)
):
    check_authorization_token_header(authorization)

    return FileResponse("/code/app/uch_distr_actual.bpmn")
