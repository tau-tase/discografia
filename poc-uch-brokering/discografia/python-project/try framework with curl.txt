# These commands enable experiments with the framework endpoint

docker build -t discografia-pythonbase ./pythonbase
docker build -t discografia-framework-endpoint ./framework-endpoint

docker run -p 80:80 discografia-framework-endpoint

curl -X POST -H "Authorization: examplekey 1234" http://localhost/authorization

curl -X POST -H "Authorization: exampletoken 1234" http://localhost/choreographies/instances/uch_brokering

curl -X GET -H "Authorization: exampletoken 1234" http://localhost/choreographies/models/uch_brokering

curl -X GET -H "Authorization: exampletoken 1234" http://localhost/brokers/mqtt
