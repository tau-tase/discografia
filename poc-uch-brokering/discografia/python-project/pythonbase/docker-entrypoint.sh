#!/bin/sh

# Inspired from: https://stackoverflow.com/questions/63198731/how-to-use-wait-for-it-in-docker-compose-file

# Abort execution if any error occurs
set -e

# Wait for the dependency to respond before proceeding
if [ -n "$DEPENDENCY_HOST" ]; then
  /pythonbase/wait-for-it/wait-for-it.sh "$DEPENDENCY_HOST:$DEPENDENCY_PORT"
fi

# Run the CMD command in Dockerfile
exec "$@"
