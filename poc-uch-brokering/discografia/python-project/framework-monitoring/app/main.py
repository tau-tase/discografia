# SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
#
# SPDX-License-Identifier: MIT

# This service monitor to all traffic that happens in the choreographies.
# In an actual product, the monitoring could visualize the progress
# of the choreographies.

import datetime
import os
import paho.mqtt.client as pahoclient


def log(message):
    print("[Monitoring] " + message)

def on_connect(client, userdata, flags, rc):
    log("Connected with result code " + str(rc))

    # Subscribe on connect and reconnect too
    pattern = "discografia/#"
    client.subscribe(pattern)
    log("Subscribed with topic pattern: '" + pattern + "'")


def on_message(client, userdata, msg):
    log(str(datetime.datetime.now()) + " " + msg.topic + ": " + msg.payload.decode())

# Get framework endpoint from environment variable, or use localhost if not set
mqtt_host = os.environ.get("DEPENDENCY_HOST")

if (mqtt_host is None):
    mqtt_host = "127.0.0.1"

log("Hello from monitoring service!")

client = pahoclient.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(mqtt_host, 1883, 60)

# Continue execution until killed (?)
client.loop_forever()
