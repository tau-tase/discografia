# SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
#
# SPDX-License-Identifier: MIT

import dateutil.parser as dateparser
import os
import json
import requests
import message_broker_client as message_broker_client


def log(msg: str):
    print("[{0}] {1}".format(party_id, msg))


party_id = os.environ["PARTY_ID"]

# Building all topic strings here
TOPIC_SUBSTRING_AGGR = "Aggregator"
TOPIC_SUBSTRING_DSO = "Dso"
topic_substring_aggr_offer_request = TOPIC_SUBSTRING_AGGR + "/OfferRequest"
topic_substring_aggr_offer_response = TOPIC_SUBSTRING_AGGR + "/OfferResponse/" + party_id
topic_substring_aggr_reservation_accept_request = TOPIC_SUBSTRING_AGGR + "/ReservationAcceptRequest/" + party_id
topic_substring_aggr_reservation_accept_response = TOPIC_SUBSTRING_AGGR + "/ReservationAcceptResponse/" + party_id
topic_substring_aggr_reservation_reject_request = TOPIC_SUBSTRING_AGGR + "/ReservationRejectRequest/" + party_id
topic_substring_dso_net_behavior = TOPIC_SUBSTRING_DSO + "/NetBehavior"

# Inferring the address of the dynamic pricing service from the party name
dynamic_pricing_url = "http://dynamic-pricing-" + party_id[-1].lower() + ":9001/availability/"


def do_client_setup():
    
    framework_host = os.environ["DEPENDENCY_HOST"]
    framework_url = "http://" + framework_host + ":80"

    # Authorize with choreo framework
    auth_data = requests.post(
        url = framework_url + "/authorization",
        headers = {"Authorization": "examplekey-customer1"}
    ).json()

    log("Authorized with token " + auth_data["token"])

    # Get MQTT broker information
    broker_info = requests.get(
        url = framework_url + "/brokers/mqtt",
        headers = {"Authorization": auth_data["token"]}
    ).json()

    log("MQTT broker host is: " + broker_info["host"])

    my_client = message_broker_client.MessageBrokerClient(logger_fcn = log)
    my_client.setup(mqtt_host = broker_info["host"], listen_to =
    [   
        # The wildcard '+' enables receiving any request
        # regardless of the correlation ID.
        topic_substring_aggr_offer_request,
        topic_substring_aggr_reservation_accept_request,
        topic_substring_aggr_reservation_reject_request
        # TODO: get data from DSO?
    ])

    return my_client



def receive_offer_request(my_client):

    # Receiving an offer request
    offer_request = my_client.try_latest_message([
        topic_substring_aggr_offer_request
        ])

    if (offer_request is None):
        raise Exception("Offer request not received!")
    
    return offer_request



def iso_date_to_unix_time(iso_str: str) -> float:
    
    parsed_datetime = dateparser.parse(iso_str)
    return parsed_datetime.timestamp()



def do_dynamic_pricing(offer_request):

    # Executing dynamic pricing.
    # Conceptually, this is an internal part of each aggregator.
    # However, this logic is shared by the aggregators in this demo.

    energy_demand_kwh = (offer_request["soc_target"] - offer_request["soc_arrival"]) * offer_request["battery_capacity_kwh"]

    dynamic_prices_request_dict = {
        "estimate_arrival_time": iso_date_to_unix_time(offer_request["time_arrival"]),
        "estimate_departure_time": iso_date_to_unix_time(offer_request["time_departure"]),
        "query_resolution": 300, # in seconds
        "energy_demand": energy_demand_kwh * 3600 # kWh to kilowatt seconds
        }
    
    log("Sending dynamic pricing request: " + json.dumps(dynamic_prices_request_dict))
    dynamic_prices_response = requests.post(url = dynamic_pricing_url, json = dynamic_prices_request_dict)
    log("Got dynamic prices: " + dynamic_prices_response.text)

    return dynamic_prices_response



def do_offer_steps(my_client, dynamic_prices_response):

    # Sending the offer (i.e., dynamic prices)
    my_client.send_message(topic_substring_aggr_offer_response, json.loads(dynamic_prices_response.text))
    log("I provided my price offer!")

    # Waiting to receive either accept or reject
    accept_or_reject = my_client.try_latest_message([
        topic_substring_aggr_reservation_accept_request,
        topic_substring_aggr_reservation_reject_request])

    acceptance_status = accept_or_reject["response"]

    if (acceptance_status != "accept"):
        log("My offer was rejected! :(")
    else:
        
        log("My offer was accepted!")
        # TODO: also print which charger was reserved

        # Acknowledge the acceptance
        my_client.send_message(topic_substring_aggr_reservation_accept_response, {"response": "ok"})

        # Inform DSO about net behavior
        my_client.send_message(topic_substring_dso_net_behavior,
            {"aggregator_id": party_id, "net_behavior": "..."},
            withCorrId = False) # No correlation ID as this service can serve multiple choreography instances!



log("Hello from aggregator!")
my_client = None

try:
    
    my_client = do_client_setup()

    offer_request = receive_offer_request(my_client)
    dynamic_prices_response = do_dynamic_pricing(offer_request)
    do_offer_steps(my_client, dynamic_prices_response)

except Exception as e:
    log(str(e))

# Disconnect at the end
if my_client is not None:
    my_client.close()
