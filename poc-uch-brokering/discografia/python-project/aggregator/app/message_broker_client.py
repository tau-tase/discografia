# SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
#
# SPDX-License-Identifier: MIT

import json
import paho.mqtt.client as pahoclient


# Implements integration with the message bus.
# The basic format of topic strings is managed here, but
# the more detailed parts of these come from the outside.
class MessageBrokerClient:
    
    # It is assumed that no mutual exclusion is needed
    # now that the one single thread runs the program and 
    # calls the callbacks.
    # See https://pypi.org/project/paho-mqtt/

    __TOPIC_START_FRAMEWORK = "discografia"

    # The wildcard '+' enables receiving any request regardless of the correlation ID.
    __TOPIC_START_SUBSCRIBE = __TOPIC_START_FRAMEWORK + "/+/"


    def __init__(self, logger_fcn):

        self.__client = None
        self.__logger_fcn = logger_fcn

        # The latest messages will be stored here
        self.__latest_received_messages = {}

        self.__correlation_id = None


    def setup(self, mqtt_host: str, listen_to: list[str]):
        
        if self.__client is not None:
            raise Exception("Client already set up")

        self.__listen_to = listen_to

        # Set up the client object
        self.__client = pahoclient.Client()
        self.__client.on_connect = self.__on_connect
        self.__client.on_message = self.__on_message
        self.__client.connect(mqtt_host, 1883, 60)


    def try_latest_message(self, parts_of_topic: list[str]):
        
        if self.__client is None:
            raise Exception("Client not set up")

        # Trying at most 10 times. Each attempts ends either when
        # any message is received or when the timeout fires.
        for a in range(10):

            # Trying to receive; the callback will be called if something is received
            self.__client.loop(3) # Timeout of 3 seconds
        
            # Iterate all received latest messages and return
            # one if it matches one of the given topic name substrings
            for recv_topic in self.__latest_received_messages:

                for expected_topic in parts_of_topic:
                    if expected_topic in recv_topic: # Substring match
                        
                        # Decode JSON message into an object
                        msg_string = self.__latest_received_messages.pop(recv_topic)
                        return json.loads(msg_string)
        
        # No match was found
        return None


    def send_message(self, topic_end: str, msg, withCorrId: bool = True):
        
        if self.__client is None:
            raise Exception("Client not set up")

        # Decode and send
        full_topic = ""

        # Include correlation ID or not?
        if withCorrId:
            full_topic = "{0}/{1}/{2}".format(self.__TOPIC_START_FRAMEWORK, self.__correlation_id, topic_end)
        else:
            full_topic = "{0}/{1}".format(self.__TOPIC_START_FRAMEWORK, topic_end)

        self.__client.publish(full_topic, json.dumps(msg))


    def close(self):
        if self.__client is not None:
            self.__client.disconnect()
            self.__client = None


    def __on_connect(self, client, userdata, flags, rc):
        self.__log("Connected with result code " + str(rc))

        # This will execute on connect and reconnect too
        for s in self.__listen_to:
            full_pattern = self.__TOPIC_START_SUBSCRIBE + s
            self.__client.subscribe(full_pattern)
            self.__log("Subscribed with topic pattern: '" + full_pattern + "'")


    def __on_message(self, client, userdata, msg):
        
        self.__log("Received: " + msg.topic + ": " + msg.payload.decode())

        # Stoge the message
        self.__latest_received_messages[msg.topic] = msg.payload

        # Getting and storing the correlation ID.
        # To support concurrent processes, this should be done only
        # once when each process starts!
        self.__correlation_id = self.__get_correlation_id_from_topic(msg.topic);


    def __get_correlation_id_from_topic(self, topic: str):
        
        # Expecting topic structure "discografia/<corr_id>/..."
        parts = topic.strip("/").split("/")
        return parts[1];


    def __log(self, message: str):
        self.__logger_fcn(message)
