# Use this file to set up a virtual environment for Python
fastapi~=0.89.1
paho-mqtt~=1.6.1
pydantic~=1.10.4
requests~=2.28.2
uvicorn~=0.20.0
