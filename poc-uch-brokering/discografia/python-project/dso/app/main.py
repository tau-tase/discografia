# SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
#
# SPDX-License-Identifier: MIT

import os
import requests
import paho.mqtt.client as pahoclient


def log(message: str):
    print("[DSO] " + message)

def build_topic_name(message_type: str):
    # Note: no correlation ID, because this is not necessarily
    # bound to any specific choreography instance!
    return "discografia/Dso/" + message_type


log("Hello from DSO!")

# Get framework endpoint from environment variable, or use localhost if not set
framework_host = os.environ.get("DEPENDENCY_HOST")

if (framework_host is None):
    framework_host = "127.0.0.1"

# Build framework URL, assuming the port!
framework_url = "http://" + framework_host + ":80"

# Authorize with choreo framework
auth_data = requests.post(
    url = framework_url + "/authorization",
    headers = {"Authorization": "examplekey-customer1"}
).json()

log("Authorized with token " + auth_data["token"])

# Get MQTT broker information
broker_info = requests.get(
    url = framework_url + "/brokers/mqtt",
    headers = {"Authorization": auth_data["token"]}
).json()

log("MQTT broker host is: " + broker_info["host"])


def on_connect(client, userdata, flags, rc):
    log("Connected with result code " + str(rc))
    client.subscribe(build_topic_name("NetBehavior"))

def on_message(client, userdata, msg):
    # In this demo, the DSO does not do anything with the message
    # but just receives this
    log(msg.topic + " " + msg.payload.decode())

client = pahoclient.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(broker_info["host"], 1883, 60)
client.loop_forever()
