# SPDX-FileCopyrightText: 2023 Petri Kannisto, Tampere University <petri.kannisto@tuni.fi>
#
# SPDX-License-Identifier: MIT

import datetime
import json
import os
import time
import paho.mqtt.publish as pahopub
import paho.mqtt.subscribe as pahosub
import requests


def log(message):
    print("[Customer] " + message)


def build_topic_name(corr_id, message_type):
    # Assuming that the UCH broker has the ID "uchbroker1"
    return "discografia/" + corr_id + "/UchBroker/" + message_type + "/uchbroker1"


def do_choreo_setup():

    # Get framework endpoint from environment variable, or use localhost if not set
    framework_host = os.environ.get("DEPENDENCY_HOST")

    if (framework_host is None):
        framework_host = "127.0.0.1"

    # Build framework URL, assuming the port!
    framework_url = "http://" + framework_host + ":80"

    # Authorize with choreo framework
    auth_data = requests.post(
        url = framework_url + "/authorization",
        headers = {"Authorization": "examplekey-customer1"}
    ).json()

    log("Authorized with token " + auth_data["token"])

    # Get the choreography started
    choreo_instance_data = requests.post(
        url = framework_url + "/choreographies/instances/uch_brokering",
        headers = {"Authorization": auth_data["token"]}
    ).json()

    mqtt_host = choreo_instance_data["message_broker_host"]
    correlation_id = choreo_instance_data["correlation_id"]
    log("MQTT broker host is: " + mqtt_host)
    log("Correlation ID is: " + correlation_id)

    return mqtt_host,correlation_id



def do_request(mqtt_host: str, correlation_id: str):

    topic_request = build_topic_name(corr_id = correlation_id, message_type = "ReservationRequest")
    topic_response = build_topic_name(corr_id = correlation_id, message_type = "ReservationResponse")

    # Assuming the arrival time is now + 10 minutes and
    # the departure time 90 minutes after the arrival
    time_arrival = datetime.datetime.utcnow(). \
        replace(second=0, microsecond=0, tzinfo=datetime.timezone.utc) \
        + datetime.timedelta(minutes=10)
    time_departure = time_arrival + datetime.timedelta(minutes=90)

    # Requesting for reservation via the given MQTT broker using the given correlation ID.
    request_payload = {
        "customer_id": "customer_x",
        "battery_capacity_kwh": 110,
        "v2g_allowance_kwh": 0, # V2G not allowed
        "soc_arrival": 0.4,
        "soc_target": 0.8,
        "power_charge_kw": 11,
        "power_discharge_kw": 11,
        "time_arrival": time_arrival.isoformat(),
        "time_departure": time_departure.isoformat()
    }
    pahopub.single(topic_request, payload = json.dumps(request_payload), hostname = mqtt_host)

    log("Requested for reservation. Waiting for response...")
    response = pahosub.simple(topic_response, hostname = mqtt_host, keepalive = 120)
    log("UCH broker assigned me the charging unit: " + json.loads(response.payload)["charging_unit_id"])



log("Hello from customer!")

# Wait for a while to let the other nodes start.
# This is for the services reached with MQTT.
log("Wait to give services time to start...")
time.sleep(10) # 10 seconds

mqtt_host,correlation_id = do_choreo_setup()
do_request(mqtt_host,correlation_id)
